#include <iostream>
#include <string.h> // Pour la gestion des string
#include <Windows.h> // Pour Sleep()
#include <time.h>

using namespace std;

// ##### Prototypes #####
void DisplayAllThrow(int BreakPoint);
void DisplayLoop(int Speed, int BreakPoint = -1);
void DisplayHelloAndRules();
void ClassicRoulette(int *NbTokens);
void RussianRoulette(int *NbTokens, bool *StillAlive);
string AskRussianRoulette();

// #### variables ####
int BreakPoint = -1;
int NbTokens = 10;
string PlayerChoice;
bool ContinueGame = true;
bool StillAlive = true;

int main()
{
    srand(time(NULL));                                      // initialise l'al�atoire pour le programme
    DisplayHelloAndRules();                                 // Appel � la fonction pour afficher les r�gles

    while(ContinueGame){                                    // Continue tant que la variable est � true

        if(NbTokens > 0 && StillAlive == true){             // Si assez de jetons et toujours en vie, alors
            ClassicRoulette(&NbTokens);                     // lance la fonction de  jeux de la roulette classique

        }else if(NbTokens <= 0 && StillAlive == true){      // Si plus de jetons mais toujours en vie, alors
            RussianRoulette(&NbTokens, &StillAlive);        // lance la fonction de jeux de la roulette russe

        }else if(NbTokens >= 100 || StillAlive == false){   // Si plus de 100 jetons ou mort alors,
            ContinueGame = false;                           // quite la boucle et termine le programme
        }
    }
    return 0;
}

// Lance plusieus fois la fnc suivante et int�gre une valeur d'arr�t pour l'animation de la roulette
void DisplayAllThrow(int BreakPoint = -1)
{
    DisplayLoop(25);
    DisplayLoop(50);
    DisplayLoop(100,BreakPoint);
}

// Pour lancer l'affichage d'un lancer de roulette a diff�rentes vitesses
// Affiche tout les nombres de 0 � 36, et encadre le nombre sur lequel se trouve fictivement la boule
void DisplayLoop(int Speed, int BreakPoint)
{
    int i;
    int j;

    for(i=0; i<37; i++){
        system("cls");
        for (j=0; j<i; j++){
            if(i == 0){

            }else{
                cout << "-" << j;
            }
        }
        cout << "-[" << i << "]";
        for (j=i+1; j<37; j++){
            if(i==36){

            }else{
                cout << "-" << j;
            }
        }
        if(i==BreakPoint){
            break;
        }
        Sleep(Speed);
    }
}

// Affiche le sc�nario + les r�gles
void DisplayHelloAndRules()
{
    // Clear l'�cran si c'est une deuxi�me partie �a sera utile
    system("cls");

    cout << "Tu te reveil, gueulle de bois, assis sur une chaise" << endl;
    cout << "Deux type te surveille, batte a la main"<<  endl;
    cout << "Un des types s'avance et te dis en roulant bien ses r : hey, rembourse, ou meurt" <<  endl;
    cout << "Sur une feuille, note 100, tu dois gagner 100 jetons sinon ..." <<  endl;
}

void ClassicRoulette(int *NbTokens)
{
    // #### D�claration des variables ####
    bool PlayClassicRoulette = true;
    bool ValidBet;
    int PlayerBetValue = -1;
    int PlayerBetNumber = -1;
    int RouletteResult;

    while(PlayClassicRoulette){                                                                 // Boucle du jeux de la roulette

        ValidBet = false;                                                                       // Bool�en pour v�rifier la valid�e de la mise
        while(!ValidBet){                                                                       // Demande la mise tant qu'elle n'est pas valide

            if(PlayerBetValue > 0 && PlayerBetValue <= *NbTokens && PlayerBetValue <= 25 ){     // Si la valeur entr�e est sup�rieur � 0, et inf�rieur ou �gal aux jetons restant, et max 25
                ValidBet = true;                                                                // La mise est valide le bool�en passe a true

            }else{
                cout << "Tu a " << *NbTokens << " jetons, entre ta mise :";                      // Affiche le nombre de jetons restants
                cin >> PlayerBetValue;                                                           // R�cup�re l'entr�e de l'utilisateur
            }
        }

        ValidBet = false;                                                                         // R�-initialise le bool�en de contr�le
        while(!ValidBet){                                                                         // Boucle pour r�cup�rer un num�ro sur lequel le joueur mise

            if(PlayerBetNumber >= 0 && PlayerBetNumber < 37 ){                                    // Si nombre entr� sup�rieur/�gal � et inf�rieur � 37, alors
                ValidBet = true;                                                                  // la valeur et valide et la variable de contr�le passe � true

            }else{

                cout << "Choisis le num�ro sur lequel tu mise :";
                cin >> PlayerBetNumber;                                                            // R�cup�re l'entr�e de l'utilisateur
            }
        }

        RouletteResult = (rand()%37);                                                               // Donne une valeur al�atoire entre 0 et 37 � la variable
        cout <<  RouletteResult << endl;

        DisplayAllThrow(RouletteResult);                                                            // Lance l'animation
        Sleep(500);                                                                                 // met en pause le programme pour 500 milisecondes

        if(RouletteResult == PlayerBetNumber){                      // si le joueur � mis� le bon nombre, alors
            *NbTokens += (PlayerBetValue*2);                        // il gagne le double de sa mise
            cout << "Tu a gagner le double de ta mise" << endl;

        }else{
            *NbTokens -= PlayerBetValue;                            // sinon il perd sa mise
            cout << endl;
            cout << "Tu a perdu ta mise" << endl;
        }

        Sleep(2500);                                                 // met en pause le programme pour laisser le joueur contempler sa d�faite
        PlayClassicRoulette = false;                                 // la variable passe � false et fais quiter la fonction jeux de la roulette
    }
}

// fonction jeux de la roulette russe
void RussianRoulette(int *NbTokens, bool *StillAlive){

    int BarrelTable[6];                                                         // Cr�er la tableau qui repr�sente le barilet
    int Bullet = (rand()%6);                                                    // Met dans une variable une valeur entre 0 et 5 qui sera l'emplacement de la balle
    int i;
    for(i=0; i<6; i++){                                                         // Remplis le tableau de 0
        BarrelTable[i] = 0;
    }
    BarrelTable[Bullet] = 1;                                                    // Place la balle

    string UserChoice;
    bool ValidChoice = false;                                                   // initialise la variable de contr�le

    cout << " La c'est la roulette russe bonhomme, un tir minimum " << endl;
    cout << "o pour tirer" << endl;

    while(!ValidChoice)                                                         // boucle pour obtenir un "o" et continuer
    {
        if(UserChoice == "o"){
            ValidChoice = true;
        }else{
            cin >> UserChoice;
        }
    }

    UserChoice = "";                                                            // R�-initialisation du UserChoice
    while(*StillAlive == true && UserChoice != "n" )                            // boucle du jeux,
    {
        if(BarrelTable[0] == 1){                                                // si la balle est dans table[0] c'est comme si elle �tait dans la chambre et boum crever
            *StillAlive = false;
            cout << "Boum t'es mort" << endl;

        }else{                                                                  // sinon,
            *NbTokens += 20;                                                    // le joueur gagne 20 jetons
            cout << "Tu n'est pas mort,tu gagne 20 jetons ,o pur tirer encore, n pour retourner a la roulette" << endl;
            UserChoice = AskRussianRoulette();                                  // Boucle pour obtenir un choix, continuer ou retourner � la roulette

            if(UserChoice == "o"){                                              // si le joueur continue on d�-cr�mente l'emplacement de la  balle dans le tableau
                BarrelTable[Bullet] = false;
                Bullet -= 1;
                BarrelTable[Bullet] = true;
            }
        }
    }
}

// fonction qui renvoie "o" ou "n" uniquement qui sera n�c�ssaire pour continuer ou pas la roulette russe
string AskRussianRoulette()
{
    bool ValidChoice = false;
    string UserChoice = "";

    while(!ValidChoice)
    {
        if(UserChoice == "o" || UserChoice == "n" ){
            ValidChoice = true;
        }else{
            cin >> UserChoice;
        }
    }
    return UserChoice;
}
